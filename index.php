<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s01-a1</title>
</head>
<body>
    <h1>Letter-Based Grading</h1>
    <p><?php echo getLetterGrade(98); ?></p>
    <p><?php echo getLetterGrade(87); ?></p>
    <p><?php echo getLetterGrade(101); ?></p>
    
    <p> git add .
        git commit -m "Commit message"
        git push origin master</p>
    
</body>
</html>